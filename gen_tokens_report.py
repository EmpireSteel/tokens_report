# coding=utf-8
import datetime
from zapi import ZabbixApi
import sys
import os
from send_mail import send_report



def get_items_from_zabbix(hostname, user, password):
    z = ZabbixApi(hostname, user, password)
    items = z.zabbix_api_request(method='item.get',
                                    params={
                                        "search": {"name": "Количество USB-токенов*"},
                                        "output": ["name", "itemid", 'hostid', 'lastvalue'],
                                        'selectHosts': ["host"],
                                        'searchWildcardsEnabled': True,
                                        'templated': False
                                    })

    z.logout()
    res = {}
    for i in items:
        if "-DEB" in i['hosts'][0]['host'][-4:] or i['hosts'][0]['host'][-2:] == "NQ":
            continue
        elif i['hosts'][0]['host'][-2:] == "GK":
            host = i['hosts'][0]['host'][-7:-3]
        else:
            host = i['hosts'][0]['host'][-4:]
        lv = i['lastvalue']
        name = i['name']
        if host not in res:
            res[host] = {'Rutoken': '0', 'JaCarta': '0'}
        if 'Rutoken' in name:
            res[host]['Rutoken'] = lv
        elif 'JaCarta' in name:
            res[host]['JaCarta'] = lv
    return res


if __name__ == '__main__':
    with open('log.log', 'w') as f:
        f.write("Getting data\n")
    fname_template = "reports/tokens_report_{0}.csv"
    data_sources = [
        {"host": "http://zabbix-head.x5.ru",
         'user': "amarkin",
         'pass': "f,shdfku",
         "ts": "BF"},
        {"host": "http://msk-dpro-app351",
         'user': "amarkin",
         'pass': "q123456",
         "ts": "TC5"},
    ]
    msg = ""
    for source in data_sources:
        res = get_items_from_zabbix(source["host"], source["user"], source["pass"])
        report_fname = fname_template.format(source['ts'])
        delim = ';'
        prev_report = {}
        today = datetime.date.today().strftime("%Y-%m-%d")
        counts = {}
        if os.path.exists(report_fname):
            with open(report_fname) as f:
                data = f.readlines()[1:]
            for l in data:
                fields = l.strip().split(delim)
                store = fields[0]
                rutoken = fields[1]
                jacarta = fields[2]
                if len(fields) == 4:
                    date = fields[3]
                else:
                    date = ''
                prev_report[store] = {'Rutoken': rutoken, 'JaCarta': jacarta, 'Date': date}
        with open(report_fname, 'w') as f:
            head = ['SAP', 'Rutoken', 'JaCarta', 'Date']
            f.write(delim.join([h for h in head]) + '\n')
            for store in sorted(res.keys()):
                if store in prev_report.keys():
                    if int(res[store]['Rutoken']) != int(prev_report[store]['Rutoken']) or int(res[store]['JaCarta']) != int(prev_report[store]['JaCarta']):
                        f.write(delim.join([store, res[store]['Rutoken'], res[store]['JaCarta'], str(today)]) + '\n')
                    else:
                        f.write(
                            delim.join([store, res[store]['Rutoken'], res[store]['JaCarta'], prev_report[store]['Date']]) + '\n')
                else:
                    f.write(
                        delim.join([store, res[store]['Rutoken'], res[store]['JaCarta']]) + '\n')
        ru_count = 0
        ja_count = 0
        zero = 0
        both = 0
        for i in res.keys():
            if int(res[i]['Rutoken']) == 0 and int(res[i]['JaCarta']) == 0:
                zero += 1
            elif int(res[i]['Rutoken']) == 1 and int(res[i]['JaCarta']) == 0:
                ru_count += 1
            elif int(res[i]['Rutoken']) == 0 and int(res[i]['JaCarta']) == 1:
                ja_count += 1
            elif int(res[i]['Rutoken']) == 1 and int(res[i]['JaCarta']) == 1:
                both += 1
        with open('log.log', 'a') as f:
            f.write("Done\nSending\n")
        msg = "Total {1}: {0}\n0: {2}\nRutoken: {3}\nJaCarta: {4}\nRutoken+JaCarta: {5}\n".format(len(res), source["ts"], zero, ru_count, ja_count, both)
        for recipient in sys.argv[1:]:
            send_report(subject="Отчет по токенам {0}.".format(source["ts"]),
                        text=msg,
                        to=recipient,
                        fname=report_fname)
        with open('log.log', 'a') as f:
            f.write("Done\n")
